class Main inherits IO {
    main(): Object {
        let hello: String <- " ",
            number: Int <- 0,
            ending: String <- "\n"
        in {
            out_string("Please enter your number:\n");
            number <- in_int();
            while(2<=number) loop{
                hello<-binary(number).concat(hello);
                number<-number/2;
            }pool;
            hello<-binary(number).concat(hello);
            out_string(hello.concat(ending));
        }
    };
    binary(number:Int):String{
        if((number-(number/2*2))=0) then "0" else "1" fi
    };
};